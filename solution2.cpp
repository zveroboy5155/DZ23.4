//
// Created by german on 26.04.2022.
//

#include "solution2.h"
#include <iostream>


#define CALL_FUNC(suff) carriage##_##suff
#define CARRIAGE_NUM 10
#define PRINT_TEXT(text) std::cout << text << std::endl
#define CYCLE(body) for(int i = 0; i < CARRIAGE_NUM; i++){body;}


bool carriage_full(int num){
    return (num > 20);
}

bool carriage_emptu(int num){
    return (num == 0);
}

void sol2(){
    int train[CARRIAGE_NUM], sum = 0;
    //std::cout << " Задача в разработке" << std::endl;
    CYCLE(PRINT_TEXT("Введите количество людей в вагоне "<< i + 1 <<"");
    std::cin >> train[i];sum+=train[i])
    CYCLE(if(CALL_FUNC(full)(train[i])) PRINT_TEXT("Вагон "<< i + 1 <<" переполнен");
    if(CALL_FUNC(emptu)(train[i])) PRINT_TEXT("Вагон "<< i + 1 <<" пуст"))
    PRINT_TEXT("Всего в поезде "<< sum <<" пассажиров");
}

