#include "./solution1.h"
#include <iostream>

#define MONDAY 1
#define TUESDAY 2
#define WEDNESDAY 3
#define THURSDAY 4
#define FRIDAY 5
#define SATURDAY 6
#define SUNDAY 7

#define MONDAY_PRINT "понедельник"
#define TUESDAY_PRINT "вторник"
#define WEDNESDAY_PRINT "среда"
#define THURSDAY_PRINT "четверг"
#define FRIDAY_PRINT "пятницаа"
#define SATURDAY_PRINT "суббота"
#define SUNDAY_PRINT "воскресенье("

#define PRINT(day) std::cout << day##_##PRINT << std::endl

void sol1(){
    int input;

    std::cout << "Введите номер дня недели" << std::endl;
    std::cin >> input;
    switch (input) {
        case MONDAY:
            PRINT(MONDAY);
            break;
        case TUESDAY:
            PRINT(TUESDAY);
            break;
        case WEDNESDAY:
            PRINT(WEDNESDAY);
            break;
        case THURSDAY:
            PRINT(THURSDAY);
            break;
        case FRIDAY:
            PRINT(FRIDAY);
            break;
        case SATURDAY:
            PRINT(SATURDAY);
            break;
        case SUNDAY:
            PRINT(SUNDAY);
            break;
        default:
            break;
    }
}