#include <iostream>

#include "solution3.h"

#define SPRING 0
#define SUMMER 0
#define AUTUMN 0
#define WINTER 0

void sol3(){
#if !SPRING & !SUMMER & !AUTUMN & !WINTER
    std::cout << "Похоже мы в космосе.." << std::endl;
#elif SPRING
    std::cout << "Весна активирована" << std::endl
#elif SUMMER
    std::cout << "Лето активировано" << std::endl;
#elif AUTUMN
    std::cout << "Осень активирована" << std::endl;
#elif WINTER
    std::cout << "Зима активирована" << std::endl;
#endif
}